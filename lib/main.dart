library main;

import 'package:flutter/material.dart';

part 'src/pages/second.dart';
part 'src/pages/third.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // убираем метку режима отладки
      debugShowCheckedModeBanner: false,
      title: 'New Application',
      initialRoute: '/',
      routes: {
        '/': (BuildContext context) => MyHomePage(),
        '/second': (BuildContext context) => MySecondPage(),
        '/third': (BuildContext context) => MyThirdPage(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // шапка приложения
      appBar: AppBar(
        title: Text('Домашняя страница'),
      ),
      body: SafeArea(
        child: SafeArea(
          left: true,
          top: true,
          right: true,
          bottom: true,
          minimum: EdgeInsets.all(16.0),
          child: Center(
            child: Column(
              children: [
                RaisedButton(
                  onPressed: () {
                    Navigator.pushNamed(context, '/second');
                  },
                  child: Text('На вторую страницу'),
                ),
                RaisedButton(
                  onPressed: () {
                    Navigator.pushNamed(context, '/third');
                  },
                  child: Text('На третью страницу'),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
